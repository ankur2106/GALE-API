from authentication.models import *
import serpy

class AppFeatureListSerializer(serpy.Serializer):
   feature_name = serpy.Field()
   app_feature_order = serpy.Field()
   app_feature_id = serpy.Field()
   app_feature_parent_id = serpy.Field()
   feature_icon = serpy.Field()
   app_feature_desc = serpy.Field()