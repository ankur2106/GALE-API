from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token,refresh_jwt_token
import views
#from models import *
# from django.views.decorators.csrf import csrf


urlpatterns = (
	url(r'^login$',obtain_jwt_token),
	# url(r'^getUserDetails/$',views.getUserDetails),	
	# url(r'^example_view/$', views.example_view, name='example_view'),
	url(r'^token-refresh/$', refresh_jwt_token),
	url(r'^logout/$',views.Logout),
	# url(r'^changepassword/$',views.changePassword),
)
