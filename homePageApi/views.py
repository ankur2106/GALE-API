from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import render
from homePageApi.models import *
from sqlalchemy.orm import *
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
import sqlalchemy, sqlalchemy.orm
from sqlalchemy.dialects.postgresql import array
from django.http import JsonResponse
from datetime import datetime
import collections
from sqlalchemy.pool import NullPool
from sqlalchemy.sql.functions import coalesce
from datetime import datetime, timedelta
from datetime import *
from rom import util
import requests
from django.db import connection
# below are required if need to check any api without auth
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view,authentication_classes,permission_classes

from lxml import html
import validators
import json
import httplib
import urllib
# saves Location and latitude longitude of search made by user


import serpy

class webCrawlSeializer(serpy.Serializer):
	links=serpy.Field()
	image_list=serpy.Field()
	
class webCrwaler:
	def __init__(self,starting_url,depth):
		self.starting_url=starting_url
		self.depth=depth
		self.current_depth =0
		self.depth_links=[]
		self.apps=[]

	def crawl(self):
		app = self.get_app_from_link(self.starting_url)
		self.apps.append(app)
		self.depth_links.append(app.links)
		
		while self.current_depth < self.depth:
			current_links=[]
			for link in self.depth_links[self.current_depth]:
				if(validators.url(link)):
					print "##############",link
					current_app=self.get_app_from_link(link)
					if(current_app != []):
						current_links.extend(current_app.links)
						self.apps.append(current_app)
			self.current_depth += 1
			self.depth_links.append(current_links)
			print "=================",len(self.depth_links)

		
	def get_app_from_link(self,link):
		try:
			start_page=requests.get(link)
			tree=html.fromstring(start_page.text.encode('UTF-8'))
			links=tree.xpath('//a/@href')
			image_list=tree.xpath('//img/@src')
	
			app=App(link,links,image_list)
			return app
		except:
			print ">>>>>>>>>>>>>>>>>",link
			return []
			
		
class App:
	def __init__(self,link,links,image_list):
		self.link=link
		self.links=links
		self.image_list=image_list
		
	# def __str__(self):
		# return ("name"+self.links)
		

@api_view(['GET', 'POST'])
@permission_classes((AllowAny,))
def fetchWebCrawling(request):
	urlLink = request.GET.get('urlLink')
	urlDepth = int(request.GET.get('urlDepth'))
	if(validators.url(urlLink)):
		crawler = webCrwaler(urlLink,urlDepth)
		crawler.crawl()
		
		output=[]
		for app in crawler.apps:
			dict={}
			dict['links']=[]
			dict['image_list']=[]
			dict['own_link']=app.link
			for each_link in app.links:
				if ('http' in each_link):
					# print "*************",each_link
					# app.links.remove(each_link)
					dict['links'].append(each_link)
				# else:
					
					# print "++++++++++++",each_link
					
			for each_img in app.image_list:
				# try:
				if ('http' in each_img):
					dict['image_list'].append(each_img)
				# except:
					# print each_img
					
			output.append(dict)
		
		return Response(output)
	else:
		return Response('error')
