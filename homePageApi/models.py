from django.db import models
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import *
from sqlalchemy import types
from django.contrib.auth.models import AbstractUser
Base = declarative_base()	

	
class User(AbstractUser):
	id = models.IntegerField(primary_key=True)
	first_name = models.CharField(max_length=254)
	last_name = models.CharField(max_length=254)
	class Meta:
		db_table = 'auth_user'